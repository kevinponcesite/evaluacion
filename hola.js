const formulario = document.getElementById('form1');
const inputs = document.querySelectorAll('#form1 input');

//se inician las expresiones regulares
const expresiones = {
    codigo: /^([a-zA-Z0-9]){1,5}$/, 
    marca: /^([a-zA-Z0-9]){1,50}$/, 
    modelo: /^([a-zA-Z0-9]){1,30}$/, 
    ano: /^([0-9]){1,4}$/
}

const validarFormulario = (e) => {
    switch (e.target.name) {
        //Validacion de codigo
        case "codigo":
            if (expresiones.codigo.test(e.target.value)) {
                document.getElementById("c-error").innerHTML = "";
                document.getElementById("s-error").innerHTML = "";
            } else {
                document.getElementById("c-error").innerHTML = "Codigo no válido";
            }
            break;
        //Validacion de marca
        case "marca":
            if (expresiones.marca.test(e.target.value)) {
                document.getElementById("m1-error").innerHTML = "";
                document.getElementById("s-error").innerHTML = "";
            } else {
                document.getElementById("m1-error").innerHTML = "Marca no válido";
            }
            break;
        //Validacion de modelo
        case "modelo":
            if (expresiones.modelo.test(e.target.value)) {
                document.getElementById("m2-error").innerHTML = "";
                document.getElementById("s-error").innerHTML = "";
            } else {
                document.getElementById("m2-error").innerHTML = "Modelo no válido";
            }
            break;
        //Validacion de año
        case "ano":
            if (expresiones.ano.test(e.target.value)) {
                document.getElementById("a-error").innerHTML = "";
                document.getElementById("s-error").innerHTML = "";
            } else {
                document.getElementById("a-error").innerHTML = "Año no válido";
            }
            break;
        //Validacion de fecha inicio
        case "fecha1":
            fecha1 = document.getElementById('fecha1').value;
            
            if (fecha1 == "") {
                document.getElementById("f1-error").innerHTML = "Fecha no válido";
            }else{
                document.getElementById("f1-error").innerHTML = "";
                document.getElementById("s-error").innerHTML = "";
            }
            break;
        //Validacion de fecha final
        case "fecha2":
            fecha1 = document.getElementById('fecha1').value;
            fecha2 = document.getElementById('fecha2').value;
            
            if (fecha2 == "") {
                document.getElementById("f2-error").innerHTML = "Fecha no válido";
            }else{
                document.getElementById("f2-error").innerHTML = "";
                document.getElementById("s-error").innerHTML = "";
                if (fecha1 >= fecha2) {
                    document.getElementById("f2-error").innerHTML = "La fecha de inicio no puede ser mayor a la fecha final.";
                }else{
                    document.getElementById("f2-error").innerHTML = "";
                    document.getElementById("s-error").innerHTML = "";
                }
            }
            break;
    }
}

//keyup, es cuando se presiona una tecla hace el bucle.
//blur, cuando se perde el foco hace el bucle tambien.
inputs.forEach((input) => {
    input.addEventListener('keyup', validarFormulario);
    input.addEventListener('blur', validarFormulario);
});

//para no enviar espacios en blanco
formulario.addEventListener('submit', (e) => {
    e.preventDefault();
    var codigo = document.getElementById('codigo').value;
    var marca = document.getElementById('marca').value;
    var modelo = document.getElementById('modelo').value;
    var ano = document.getElementById('ano').value;
    var fechainicial = document.getElementById('fecha1').value;
    var fechafin = document.getElementById('fecha2').value;
    if (!codigo.trim("") || !marca.trim("") || !modelo.trim("") || !ano.trim("")
        || !fechainicial.trim("") || !fechafin.trim("")) {
        document.getElementById("s-error").innerHTML = "Ingrese todos los campos";
    } else {
        location.reload();
        document.form1.submit();
    }
});

